package org.cuatrovientos.java.oop.devices;

public class Computer extends Device {

	private String processor;

	public Computer(String name, String brand, float price, String processor) {
		super(name, brand, price);
		this.processor = processor;
	}

	@Override
	public String toString() {
		return super.toString() + "Computer [processor=" + processor + "]";
	}
}
