package org.cuatrovientos.java.oop.devices;

public class Device {

	protected String name;
	protected String brand;
	protected float price;

	public Device() {
		super();
	}

	public Device(String name, String brand, float price) {
		super();
		this.name = name;
		this.brand = brand;
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Device [name=" + name + ", brand=" + brand + ", price=" + price + "]";
	}

}
