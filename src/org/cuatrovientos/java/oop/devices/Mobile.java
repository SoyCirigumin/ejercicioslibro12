package org.cuatrovientos.java.oop.devices;

public class Mobile extends Device {

	private String number;

	public Mobile(String name, String brand, float price, String number) {
		super(name, brand, price);
		this.number = number;
	}

	@Override
	public String toString() {
		return super.toString() + "Mobile [number=" + number + "]";
	}

	public void call(String number) {
		System.out.println("Calling number: " + number);
	}

}
