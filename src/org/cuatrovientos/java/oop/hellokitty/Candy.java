package org.cuatrovientos.java.oop.hellokitty;

import java.util.Random;

public class Candy extends Food {

	private int calories;

	public Candy(String name, float weight, int calories) {
		super(name, weight);
		this.calories = calories;
	}

	@Override
	public String toString() {
		return super.toString() + " Candy [calories=" + calories + "]";
	}

	public int getNutriotionalValue() {
		Random rnd = new Random();
		return rnd.nextInt(6);
	}

}
