package org.cuatrovientos.java.oop.hellokitty;

import java.util.Random;

public class Fruit extends Food {
	private String vitamin;

	public Fruit(String name, float weight, String vitamin) {
		super(name, weight);
		this.vitamin = vitamin;
	}

	@Override
	public String toString() {
		return super.toString() + " Fruit [vitamin=" + vitamin + "]";
	}

	public int getNutriotionalValue() {
		Random rnd = new Random();
		return rnd.nextInt(6) + 6;
	}

}
