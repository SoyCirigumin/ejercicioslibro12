package org.cuatrovientos.java.oop.hellokitty;

public abstract class Food {

	protected String name;
	protected float weight;

	public Food(String name, float weight) {
		super();
		this.name = name;
		this.weight = weight;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	@Override
	public String toString() {
		return "Food [name=" + name + ", weight=" + weight + "]";
	}

	public abstract int getNutriotionalValue();
}
