package org.cuatrovientos.java.oop.hellokitty;

import java.util.Vector;

public class Basket {
	private Vector<Food> cesta;

	public Basket() {
		this.cesta = new Vector<Food>();
	}

	public void addFood(Food food) {
		this.cesta.add(food);
	}

	public float totalWeight() {
		int i = 0;
		float total = 0;
		while (i < this.cesta.size()) {
			total = total + ((Food) cesta.elementAt(i)).getWeight();
			i++;
		}
		return total;
	}

	@Override
	public String toString() {
		String cad="\n";
		int i=0;
		while (i < this.cesta.size()) {
			cad = cad+ ((Food) cesta.elementAt(i)).toString()+"\n";
			i++;
		}
		return cad;
	}

}
