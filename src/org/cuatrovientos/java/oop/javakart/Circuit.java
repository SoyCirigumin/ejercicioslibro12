package org.cuatrovientos.java.oop.javakart;

public class Circuit {

	private String name;
	private int length;

	public Circuit(String name, int length) {
		super();
		this.name = name;
		this.length = length;
	}

	@Override
	public String toString() {
		return "Circuit [name=" + name + ", length=" + getLength() + "]";
	}

	public int getLength() {
		return length;
	}

}
