package org.cuatrovientos.java.oop.javakart;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

public class Race {
	private String name;
	private Circuit circuit;
	private Hashtable<Vehicle, Integer> posicion;

	public Race(String name, Circuit circuit) {
		this.name = name;
		this.circuit = circuit;
		this.posicion = new Hashtable<Vehicle, Integer>();
	}

	public void addVehicle(Vehicle v) {
		posicion.put(v, 0);
	}

	public void run() {
		int i = 0;
		int j = 0;
		int contador = 0;
		boolean gano = false;
		Set<Vehicle> setOfKeys = posicion.keySet();
		while (contador < this.circuit.getLength()) {
			Iterator<Vehicle> itr = setOfKeys.iterator();
			while (itr.hasNext() && !gano) {
				Vehicle coche = itr.next();
				i = coche.move() + coche.manoeuvre();
				if (i + posicion.get(coche) >= this.circuit.getLength()) {
					gano = true;
					i = i + posicion.get(coche);
					System.out
							.println("Gano " + coche.toString() + "en movimiento " + j + " con una distancia de " + i);
				} else {
					i = i + posicion.get(coche);
					posicion.put(coche, i);
					System.out.println("Movimiento " + j + " " + coche.toString() + i);
				}
				if (i > contador) {
					contador = i;
				}
			}
			j++;
		}

	}

	@Override
	public String toString() {
		return "Race [name=" + name + ", circuit=" + circuit + ", posicion=" + posicion + "]";
	}
}
