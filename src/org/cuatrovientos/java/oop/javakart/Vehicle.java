package org.cuatrovientos.java.oop.javakart;

import java.util.Random;

public class Vehicle {

	protected String name;
	protected int speed;
	protected int acceleration;
	protected int grip;

	public Vehicle(String name) {
		super();
		this.name = name;
		this.init();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getAcceleration() {
		return acceleration;
	}

	public void setAcceleration(int acceleration) {
		this.acceleration = acceleration;
	}

	public int getGrip() {
		return grip;
	}

	public void setGrip(int grip) {
		this.grip = grip;
	}

	@Override
	public String toString() {
		return "Vehicle [name=" + name + ", speed=" + speed + ", acceleration=" + acceleration + ", grip=" + grip + "]";
	}

	private void init() {

		Random rnd = new Random();
		int cant = 0;
		while (cant < 18) {
			int puntos = rnd.nextInt(2) + 1;
			if (cant + puntos <= 18) {
				switch (rnd.nextInt(3)) {
				case 0: {
					if (this.acceleration + puntos <= 8) {
						this.acceleration = this.acceleration + puntos;
						cant = cant + puntos;
						break;
					}
				}
				case 1: {
					if (this.grip + puntos <= 8) {
						this.grip = this.grip + puntos;
						cant = cant + puntos;
						break;
					}
				}
				default: {
					if (this.speed + puntos <= 8) {
						this.speed = this.speed + puntos;
						cant = cant + puntos;
					}
					break;
				}
				}
			} else if (this.grip + this.speed > 14) {
				this.acceleration = 18 - cant;
				cant = 18;
			} else if (this.acceleration + this.grip > 14) {
				this.speed = 18 - cant;
				cant = 18;
			} else if (this.acceleration + this.speed > 14) {
				this.grip = 18 - cant;
				cant = 18;
			}
		}
	}

	public int move() {
		Random rnd = new Random();
		return this.speed + this.acceleration + rnd.nextInt(6);
	}

	public int manoeuvre() {
		Random rnd = new Random();
		return this.grip + rnd.nextInt(6);
	}

}
