package org.cuatrovientos.java.oop.school;
import java.util.Arrays;

public class Teacher extends Person {

	private String degree;
	private String[] subject;

	public Teacher(String name, int age, String degree, String[] subject) {
		super(name, age);
		this.degree = degree;
		this.subject = subject;
	}

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public String[] getSubject() {
		return subject;
	}

	public void setSubject(String[] subject) {
		this.subject = subject;
	}

	@Override
	public String toString() {
		return "Teacher [degree=" + degree + ", subject=" + Arrays.toString(subject) + ", name=" + name + ", age=" + age
				+ "]";
	}


}
