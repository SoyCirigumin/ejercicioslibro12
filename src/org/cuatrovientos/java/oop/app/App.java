package org.cuatrovientos.java.oop.app;
import org.cuatrovientos.java.oop.hellokitty.Basket;
import org.cuatrovientos.java.oop.hellokitty.Candy;
import org.cuatrovientos.java.oop.hellokitty.Fruit;
import org.cuatrovientos.java.oop.javakart.Car;
import org.cuatrovientos.java.oop.javakart.Circuit;
import org.cuatrovientos.java.oop.javakart.Race;
import org.cuatrovientos.java.oop.school.Student;
import org.cuatrovientos.java.oop.school.Teacher;
import org.cuatrovientos.java.oop.war.Soldier;
import org.cuatrovientos.java.oop.war.Tank;

public class App {

	public static void main(String[] args) {
		// Ejercicio 2:
		System.out.println("Ejercicio 2:");
		String[] asignaturas = { "Matematicas", "Lengua", "Fisica" };
		Teacher teacher = new Teacher("Maestro Astilla", 76, "Segundo grado", asignaturas);
		Student student = new Student("Pequeño Saltamontes", 39, "Priemro", "Quinto grado");
		System.out.println("Profesor: " + teacher.toString());
		System.out.println("Alumno: " + student.toString());
		// Ejercicio 3:
		System.out.println("Ejercicio 3:");
		Tank tank = new Tank("Montaña", "Pasa Frio", 50, 25, "Todo Terreno");
		Soldier soldier = new Soldier("Playa", "Toma el Sol", "Pipiolin", 19);
		System.out.println(tank.toString());
		System.out.println(soldier.toString());
		// Ejercicio 4:
		System.out.println("Ejercicio 4:");
		Basket basket = new Basket();
		Fruit fruit = new Fruit("Manzana", 5.3F, "Vitamina D");
		Fruit fruit1 = new Fruit("Naranja", 2.1F, "Vitamina C");
		Candy candy = new Candy("Chocolate", 3.7F, 100);
		Candy candy1 = new Candy("Torrija", 6.8F, 50);
		basket.addFood(fruit);
		basket.addFood(fruit1);
		basket.addFood(candy);
		basket.addFood(candy1);
		System.out.println("El peso total de la cesta es: " + basket.totalWeight());
		System.out.println("El contenido de la cesta es: " + basket.toString());
		// Ejercicio 5:
		System.out.println("Ejercicio 5:");
		Car coche1 = new Car("Ferrari", "Carlos Sainz");
		Car coche2 = new Car("Mercedes", "Hamilton lloron");
		Car coche3 = new Car("Aston Martin", "Fernando Alonso");
		Circuit circuit = new Circuit("Laguna Seca", 100);
		Race race =new Race("Rompe Huesos", circuit);
		race.addVehicle(coche1);
		race.addVehicle(coche2);
		race.addVehicle(coche3);
		System.out.println(race.toString());
		race.run();
		

	}

}
