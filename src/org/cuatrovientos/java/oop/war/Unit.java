package org.cuatrovientos.java.oop.war;

import java.util.Random;

public class Unit {

	protected String division;
	protected String name;

	public Unit() {
		super();
	}

	public Unit(String division, String name) {
		super();
		this.division = division;
		this.name = name;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Unit [division=" + division + ", name=" + name + "]";
	}
	
	public int fire() {
		Random rnd = new Random();
		return rnd.nextInt(15);
	}
	
	public int move() {
		Random rnd = new Random();
		return rnd.nextInt(7);
	}

}
