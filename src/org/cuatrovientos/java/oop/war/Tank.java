package org.cuatrovientos.java.oop.war;

public class Tank extends Unit {

	private int armor;
	private int ammo;
	private String model;

	public Tank(String division, String name, int armor, int ammo, String model) {
		super(division, name);
		this.armor = armor;
		this.ammo = ammo;
		this.model = model;
	}

	public int getArmor() {
		return armor;
	}

	public void setArmor(int armor) {
		this.armor = armor;
	}

	public int getAmmo() {
		return ammo;
	}

	public void setAmmo(int ammo) {
		this.ammo = ammo;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	@Override
	public String toString() {
		return super.toString() + " Tank [armor=" + armor + ", ammo=" + ammo + ", model=" + model + "]";
	}

	public void turn() {
		System.out.println("Mensaje por pantalla con la accion");
	}
}
