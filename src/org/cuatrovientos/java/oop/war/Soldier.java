package org.cuatrovientos.java.oop.war;

public class Soldier extends Unit {

	private String rank;
	private int age;

	public Soldier(String division, String name, String rank, int age) {
		super(division, name);
		this.rank = rank;
		this.age = age;

	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return super.toString() + " Soldier [rank=" + rank + ", age=" + age + "]";
	}
	
	public void prone() {
		System.out.println("Mensaje por pantalla con la accion");
	}

}
